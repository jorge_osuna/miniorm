
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dbms{
	/// <summary>
	/// @author Alonso Lopez
	/// </summary>
	public class SqlServer : IBD , IBD {

		public SqlServer() {
		}

		private SqlConnection conexion;

		private string connectionString;

		private SqlCommand command;

		private DataReader dr;

		private object escalar;

		/// <summary>
		/// @return
		/// </summary>
		public object conectar() {
			// TODO implement here
			return null;
		}

		/// <summary>
		/// @return
		/// </summary>
		public bool desconectar() {
			// TODO implement here
			return False;
		}

		/// <summary>
		/// @param tabla 
		/// @param campos 
		/// @return
		/// </summary>
		public int insertar(string tabla, List<Campo> campos) {
			// TODO implement here
			return 0;
		}

		/// <summary>
		/// @param tabla 
		/// @param campos 
		/// @param valoresNuevos 
		/// @param id 
		/// @return
		/// </summary>
		public bool actualizarUno(string tabla, List<Campo> campos, List<Campo> valoresNuevos, int id) {
			// TODO implement here
			return False;
		}

		/// <summary>
		/// @param tabla 
		/// @param campos 
		/// @param valoresNuevos 
		/// @param where 
		/// @return
		/// </summary>
		public int actualizarMuchos(string tabla, List<Campo> campos, List<Campo> valoresNuevos, string where) {
			// TODO implement here
			return 0;
		}

		/// <summary>
		/// @param tabla 
		/// @param IDs 
		/// @return
		/// </summary>
		public bool borrarPorID(string tabla, List<int> IDs) {
			// TODO implement here
			return False;
		}

		/// <summary>
		/// @param tabla 
		/// @param where 
		/// @return
		/// </summary>
		public int borrarMuchos(string tabla, string where) {
			// TODO implement here
			return 0;
		}

		/// <summary>
		/// @param tabla 
		/// @param IDs 
		/// @param order 
		/// @return
		/// </summary>
		public List<T> consultaPorID(string tabla, List<int> IDs, string order) {
			// TODO implement here
			return null;
		}

		/// <summary>
		/// @param tabla 
		/// @param where 
		/// @param order 
		/// @return
		/// </summary>
		public List<T> consulta(string tabla, string where, string order) {
			// TODO implement here
			return null;
		}

		/// <summary>
		/// @param tabla 
		/// @return
		/// </summary>
		public int totalRegistros(string tabla) {
			// TODO implement here
			return 0;
		}

		/// <summary>
		/// @param tabla 
		/// @return
		/// </summary>
		public int ultimoID(string tabla) {
			// TODO implement here
			return 0;
		}

	}
}