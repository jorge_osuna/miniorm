
public enum TipoDeRestriccion {
	PRIMARY_KEY,
	INDEX,
	FOREIGN_KEY,
	UNIQUE
}