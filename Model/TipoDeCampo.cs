
public enum TipoDeCampo {
	VARCHAR,
	CHAR,
	TEXT,
	INT,
	DOUBLE,
	BOOLEAN,
	ENUM,
	JSON,
	TINY_INT,
	DATE,
	DATE_TIME,
	TIMESTAMP,
	TIME,
	BLOB,
	DECIMAL
}