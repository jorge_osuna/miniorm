
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IBD {


	/// <summary>
	/// Mátodo abstracto para llevar a cabo la conexión, devuleve un objeto  de conexión del DBMS seleccionado, pero abstraido en un object. O se aqu ese necesita aplicar CASTING
	/// </summary>
	public abstract void conectar();

	public abstract void desconectar();

	/// <summary>
	/// @param tabla 
	/// @param campos
	/// </summary>
	public abstract void insertar(void tabla, void campos);

	/// <summary>
	/// permite actualizar un solo registro, indicado por su tabla y ID
	/// @param tabla 
	/// @param campos 
	/// @param valoresNuevos 
	/// @param id
	/// </summary>
	public abstract void actualizarUno(void tabla, void campos, void valoresNuevos, void id);

	/// <summary>
	/// Permite actualizar varios registros que cumplan un el where del parametro.
	/// @param tabla 
	/// @param campos 
	/// @param valoresNuevos 
	/// @param where
	/// </summary>
	public abstract void actualizarMuchos(void tabla, void campos, void valoresNuevos, void where);

	/// <summary>
	/// @param tabla 
	/// @param IDs
	/// </summary>
	public abstract void borrarPorID(string tabla, void IDs);

	/// <summary>
	/// @param tabla 
	/// @param where
	/// </summary>
	public abstract void borrarMuchos(void tabla, void where);

	/// <summary>
	/// @param tabla 
	/// @param IDs
	/// </summary>
	public abstract void consultaPorID(void tabla, void IDs);

	/// <summary>
	/// @param tabla 
	/// @param where 
	/// @param order
	/// </summary>
	public void consulta(void tabla, void where, void order);

	/// <summary>
	/// @param tabla
	/// </summary>
	public abstract void totalRegistros(void tabla);

	/// <summary>
	/// @param tabla
	/// </summary>
	public abstract void ultimoID(void tabla);

}