﻿using System;
using System.Collections.Generic;
using Mini_ORM;


namespace PruebasConsola
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Pruebas Consola...");
            Cliente cli = new Cliente();
            ParaPruebasReflection prueba = new ParaPruebasReflection();
            /////////////////////////////////////////////////////TU TABLA clientes  -- Where -- ///////////////////////
            List<Cliente> lista = prueba.consultaReflection<Cliente>("Customers", "Email like '%adventure%'");

            foreach (var cliente in lista)
            {
                Console.WriteLine(
                    "cliente ID:"+cliente.CustomerId+
                    ", Nombre:  "+cliente.Name+
                    ", Location:"+cliente.Location+
                    ", Email:"+cliente.Email+"."
                    );
            }
        }
    }

    //clase de Tipo para prueba
    class Cliente
    {
        public int CustomerId;
        public string Name;
        public string Location;
        public string Email;

    }
}
